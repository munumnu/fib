function fib(n) {
    if (n < 2) return n;
    var previous = 0;
    var current = 1;
    var next;
    for (var i = 1; i < n; i++) {
        next = previous + current;
        previous = current;
        current = next;
    }
    return current;
}
module.exports = fib;