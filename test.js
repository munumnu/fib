const assert = require('assert');
const fib = require('./index');

assert.strictEqual(fib(0), 0);
assert.strictEqual(fib(1), 1);
assert.strictEqual(fib(2), 1);
assert.strictEqual(fib(3), 2);
assert.strictEqual(fib(4), 3);
assert.strictEqual(fib(5), 5);
assert.strictEqual(fib(6), 8);
assert.strictEqual(fib(7), 13);
assert.strictEqual(fib(99), 218922995834555200000);
assert.strictEqual(fib(1337), 1.1667827829692572e+279);
assert.strictEqual(fib(1476), 1.3069892237633987e+308);