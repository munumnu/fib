const Benchmark = require('benchmark');
const fib = require('./index');

var suite = new Benchmark.Suite;

// add tests
suite
    .add('fib', function () {
        fib(0);
        fib(1);
        fib(2);
        fib(3);
        fib(4);
        fib(5);
        fib(6);
        fib(7);
        fib(99);
        fib(1337);
        fib(1476);
    })
    // add listeners
    .on('cycle', function (event) {
        console.log(String(event.target));
    })
    .on('complete', function () {
        console.log('Fastest is ' + this.filter('fastest').map('name'));
    })
    // run async
    .run({'async': true});